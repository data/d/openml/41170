# OpenML dataset: TUPRASBoilerData

https://www.openml.org/d/41170

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: TUPRAS,Turkish Petroleum Refineries Inc.  
**Source**: Sensor data measurements of one Boiler  
**Please cite**:   

Sensor data measurements of one Boiler, containing WaterInput/SteamOutput (flow, temperature, pressure) for one month, which is measured every minute.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41170) of an [OpenML dataset](https://www.openml.org/d/41170). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41170/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41170/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41170/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

